<!--start section-->
<section class="nicdark_section nicdark_bg_greydark">

    <!--start nicdark_container-->
    <div class="nicdark_container nicdark_clearfix">

        <div class="nicdark_space30"></div>

        <div class="grid grid_3 nomargin percentage">

            <div class="nicdark_space20"></div>

            <div class="nicdark_margin10">
                <h4 class="white">Authorised Exam <br>Preparation Centre</h4>
                <div class="nicdark_space20"></div>
                <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
                <div class="nicdark_space20"></div>
                <p class="white"><?= img('img/logocan.jpg','width:100%') ?></p>
                <div class="nicdark_space20"></div>
                <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
                <div class="nicdark_space20"></div>
                <a href="<?= base_url() ?>p/contacto.html" class="nicdark_btn_icon nicdark_bg_orange small nicdark_shadow nicdark_radius white"><i class="icon-mail-1 nicdark_rotate"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="<?= base_url() ?>p/contacto.html" class="nicdark_btn_icon nicdark_bg_yellow small nicdark_shadow nicdark_radius white"><i class="icon-home nicdark_rotate"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
                <a href="<?= base_url() ?>p/contacto.html" class="nicdark_btn_icon nicdark_bg_red small nicdark_shadow nicdark_radius white"><i class="icon-phone-outline nicdark_rotate"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;
            </div>
        </div>

        <div class="grid grid_3 nomargin percentage">

            <div class="nicdark_space30"></div>

            <div class="nicdark_marginleft10">
                <h4 class="white">LINKS D'INTERES </h4>
                <div class="nicdark_space20"></div>
                <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
            </div>
            <div class="nicdark_space10"></div>

            <a target="_blank" href="http://www.cambridgeenglish.org/learning-english/parents-and-children/" class="nicdark_btn nicdark_bg_greydark2 small nicdark_shadow nicdark_radius white subtitle nicdark_margin10">Cambridge English Kids</a>
            <a target="_blank" href="http://www.cambridgeenglish.org/learning-english/activities-for-learners/b1r005-websites-for-teens " class="nicdark_btn nicdark_bg_greydark2 small nicdark_shadow nicdark_radius white subtitle nicdark_margin10">Cambridge English Teens</a>
            <a target="_blank" href="http://learnenglishkids.britishcouncil.org/en/" class="nicdark_btn nicdark_bg_greydark2 small nicdark_shadow nicdark_radius white subtitle nicdark_margin10">British Council Kids</a>
            <a target="_blank" href="http://learnenglishteens.britishcouncil.org" class="nicdark_btn nicdark_bg_greydark2 small nicdark_shadow nicdark_radius white subtitle nicdark_margin10">British Council Teens</a>
        </div>        
        
        <div class="grid grid_6">
            <div class="nicdark_space20"></div>
            <div class="nicdark_archive1 nicdark_bg_grey nicdark_radius nicdark_shadow">
                <div class="nicdark_textevidence nicdark_bg_blue nicdark_radius_top">
                    <h4 class="white nicdark_margin20">CALENDARI</h4>
                    <i class="icon-calendar nicdark_iconbg right medium blue"></i>
                </div>
                <ul class="nicdark_list border" style="height: 180px; overflow-x: auto; overflow-y: auto;">                    
                    <?php $this->db->order_by('fecha','ASC'); ?>
                    <?php $eventos = $this->db->get_where('eventos',array('fecha >='=>date("Y-m-d"))); ?>
                    <?php foreach($eventos->result() as $e): ?>
                    <li class="nicdark_border_grey">
                            <div class="nicdark_margin20 nicdark_relative" style="width:90%">
                                <div style="width:60px;" class="nicdark_absolute nicdark_activity center">
                                    <div class="nicdark_textevidence nicdark_bg_greydark nicdark_radius_top">
                                        <h2 class="white nicdark_margin5"><?= date("d",strtotime($e->fecha)) ?></h2>
                                    </div>
                                    <div class="nicdark_textevidence nicdark_bg_blue nicdark_radius_bottom">
                                        <h6 class="white nicdark_margin5"><?= strtoupper(substr(strftime("%B",strtotime($e->fecha)),0,3)) ?></h6>
                                    </div>
                                </div>
                                <div class="nicdark_activity nicdark_marginleft80">
                                    <h5 class="greydark"><?= $e->titulo ?></h5>
                                    <div class="nicdark_space10"></div>
                                    <p><?= $e->texto ?></p>
                                </div>
                                <div style="width:99px; left:inherit; right:0" class="eventofoto nicdark_absolute nicdark_activity center">
                                    <img src='<?= base_url('uploads/eventos/'.$e->foto) ?>' style="width:100%">
                                </div>
                            </div>
                        </li>                    
                    <?php endforeach ?>
                    <?php if($eventos->num_rows()==0): ?>
                        <li>Sin eventos disponibles</li>
                    <?php endif ?>
                </ul>
            </div>
        </div>

        <div class="nicdark_space50"></div>

    </div>
    <!--end nicdark_container-->

</section>
<!--end section-->


<!--start section-->
<div class="nicdark_section nicdark_bg_greydark2 nicdark_copyrightlogo">

    <!--start nicdark_container-->
    <div class="nicdark_container nicdark_clearfix">

        <div class="grid grid_6 nicdark_aligncenter_iphoneland nicdark_aligncenter_iphonepotr">
            <div class="nicdark_space20"></div>
            <p class="white">© Copyright 2017 by <span class="grey">PEOPLE SCHOOL</span> Made With <i class="icon-heart-filled red nicdark_zoom"></i> In Igualada</p>
        </div>


        <div class="grid grid_6">
            <div class="nicdark_focus right nicdark_aligncenter_iphoneland nicdark_aligncenter_iphonepotr">
                <!--<div class="nicdark_margin10">
                    <a href="#" class="nicdark_facebook nicdark_press right nicdark_btn_icon small nicdark_radius white"><i class="icon-facebook-1"></i></a>
                </div>
                <div class="nicdark_margin10">
                    <a href="#" class="nicdark_press right nicdark_btn_icon nicdark_bg_red nicdark_shadow small nicdark_radius white"><i class="icon-gplus"></i></a>
                </div>
                <div class="nicdark_margin10">
                    <a href="#" class="nicdark_press right nicdark_btn_icon nicdark_bg_blue nicdark_shadow small nicdark_radius white"><i class="icon-twitter-1"></i></a>
                </div>-->
                <div class="nicdark_margin10">
                    <a href="#start_nicdark_framework" class="nicdark_zoom nicdark_internal_link right nicdark_btn_icon nicdark_bg_greydark2 small nicdark_radius white"><i class="icon-up-outline"></i></a>
                </div>
            </div>
        </div>

    </div>
    <!--end nicdark_container-->

</div>
