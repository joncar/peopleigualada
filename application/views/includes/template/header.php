<div class="main-preloader">
    <div class="ball-scale">
        <div></div>
    </div>
</div>
<div class="nicdark_overlay"></div>
<!--START SIDEBAR-->

<!--END SIDEBAR-->
<div class="nicdark_section nicdark_navigation fade-down header">
    <div class="nicdark_menu_boxed">
        <?php $this->load->view('includes/template/topnav'); ?>
        <div class="nicdark_space3 nicdark_bg_gradient"></div>
        <div class="nicdark_section nicdark_bg_grey nicdark_shadow nicdark_radius_bottom">
            <div class="nicdark_container nicdark_clearfix">
                <div class="grid grid_12 percentage">
                    <div class="nicdark_space20"></div>
                    <div class="nicdark_logo nicdark_marginleft10">
                        <a href="<?= site_url() ?>#home"><img alt="" src="<?= base_url() ?>img/logo.png"></a>
                    </div>                   

                    <nav>
                        <ul class="nicdark_menu nicdark_margin010 nicdark_padding50">

                            <li class="orange">
                                <a href="<?= base_url() ?>#quisom">QUI SOM?</a>
                            </li>
                            <li class="red">
                                <a href="<?= base_url() ?>#metodologia">METODOLOGIA</a>
                            </li>
                            <li class="blue">
                                <a href="<?= base_url() ?>#faq">FAQ</a>
                            </li>
                            <li class="yellow nicdark_megamenu">
                                <a href="<?= base_url() ?>#testimonio">OPINIONS</a>
                            </li>
                            <li class="yellow nicdark_megamenu" style="display: none">
                                <a href="<?= base_url() ?>#curriculum">WORK WITH US</a>
                            </li>
                            <li class="orange">
                                <a href="<?= site_url('p/contacto') ?>">CONTACTE</a>
                            </li>
                        </ul>
                    </nav>

                    <div class="nicdark_space20"></div>

                </div>

            </div>
            <!--end container-->

        </div>
        <!--end header-->

    </div>

</div>
<!--start-->
<!--start section-->
