<div class="nicdark_section nicdark_bg_greydark nicdark_displaynone_responsive">
    <div class="nicdark_container nicdark_clearfix">
        <div class="grid grid_6">
            <div class="nicdark_focus">
                <h6 class="white">
                    <i class=" icon-calendar"></i> <a class="white" href="events.html">Dilluns a divendres de 15 a 21.30h</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;<span class="grey">·</span>&nbsp;&nbsp;&nbsp;
<!--                    <i class="icon-pencil-1"></i>&nbsp;&nbsp;<a class="white" href="blog-masonry.html">NEWS</a>-->
<!--                    &nbsp;&nbsp;&nbsp;&nbsp;<span class="grey">·</span>&nbsp;&nbsp;&nbsp;&nbsp;-->
                    <i class="icon-phone-outline" ></i> <a class="white"href="tel:938045005">93 804 50 05</a>
<!--                 <span class="grey">·</span>-->



                </h6>
            </div>
        </div>
        <div class="grid grid_6 right">
            <div class="nicdark_focus right">
                <h6 class="white">
                    <i class=" icon-thumbs-up"></i> <a class="white" href="<?= base_url() ?>#curriculum">Would you like to work with us?</a>
                    &nbsp;&nbsp;&nbsp;&nbsp;<span class="grey">·</span>&nbsp;&nbsp;&nbsp;
                    <i class="icon-lock-1" style="color: rgb(255, 214, 88); "></i> 
                    <?php if(empty($_SESSION['user'])): ?>
                    <a class="white" href="<?= base_url('panel') ?>" style=" color: rgb(255, 214, 88); ">ZONA PARES</a>
                    <?php else: ?>
                    <a class="white" href="<?= base_url('blog') ?>" style=" color: rgb(255, 214, 88); "><?= $_SESSION['nombre'] ?></a>
                    &nbsp;&nbsp;&nbsp;&nbsp;<span class="grey">·</span>&nbsp;&nbsp;&nbsp;
                    <a class="white" href="<?= base_url('main/unlog') ?>" style=" color: rgb(255, 214, 88); ">Sortir</a>
                    <?php endif ?>
                </h6>
            </div>
        </div>        

    </div>
</div>
