﻿<!DOCTYPE html>
<!--[if lt IE 7 ]><html class="ie ie6" lang="en"> <![endif]-->
<!--[if IE 7 ]><html class="ie ie7" lang="en"> <![endif]-->
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
    <head>

        <meta charset="utf-8">

        <title><?= empty($title) ? 'People Language & Fun' : $title ?></title>
        <meta name="description" content="People es una escola d’angles situada a Igualada. A la nostra academia d’idiomes ens hem especialitzat en un esenyament de l'angles molt personalitzada, els resultats dels nostres alunmes en els examens de Cambrige ho demostren.">
        <meta name="keywords" content="Academia de Ingles, Academia d’Angles, Escuela de Ingles, Escola d’Angles, Idiomes a igualada, Igualada, People Language, Escola People, Academia People, Exámenes oficiales Cambridge, Examens oficials Cambridge, Angles a Igualada, People, Escuela de idiomas en igualada, Escola d'idiomes a igualada">
        <meta name="author" content="nicdark" hipo">
        <meta name="viewport" content="width=device-width, initial-scale=1.0"Academia de Ingles, Academia d’Angles, Escuela de Ingles, Escola d’Angles, Idiomes a igualada, Igualada, People Language, Escola People, Academia People, Exámenes oficiales Cambridge, Examens oficials Cambridge, Angles a Igualada, People, Escuela de idiomas en igualada, Escola d'idiomes a igualada">

        <!--START CSS--> 
        <link rel="stylesheet" href="<?= base_url() ?>css/template/nicdark_style.css"> <!--style-->
        <link rel="stylesheet" href="<?= base_url() ?>css/template/nicdark_responsive.css"> <!--nicdark_responsive-->

        <!--revslider-->
        <link rel="stylesheet" href="<?= base_url() ?>css/template/revslider/settings.css"> <!--revslider-->
        <link rel="stylesheet" href="<?= base_url() ?>css/template/owl.carousel.css"> <!--revslider-->
        <link rel="stylesheet" href="<?= base_url() ?>css/template/owl.theme.default.min.css"> <!--revslider-->

        <!--END CSS-->

        <!--google fonts-->
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'> <!-- font-family: 'Montserrat', sans-serif; -->
        <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'> <!-- font-family: 'Raleway', sans-serif; -->
        <link href='http://fonts.googleapis.com/css?family=Montez' rel='stylesheet' type='text/css'> <!-- font-family: 'Montez', cursive; -->
        <link href="https://fonts.googleapis.com/css?family=Esteban" rel="stylesheet">  <!-- font-family: 'Montez', cursive; -->
        <link href="https://fonts.googleapis.com/css?family=Arvo:400,400i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Bitter:400,400i,700" rel="stylesheet">

        <!--[if lt IE 9]>  
        <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>  
        <![endif]-->  

        <!--FAVICONS-->
        <link rel="shortcut icon" href="<?= base_url() ?>img/favicon/favicon.ico">
        <link rel="apple-touch-icon" href="<?= base_url() ?>img/favicon/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>img/favicon/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>img/favicon/apple-touch-icon-114x114.png">
        <!--END FAVICONS-->
        <!-- Global Site Tag (gtag.js) - Google Analytics -->
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-58932801-3', 'auto');
            ga('send', 'pageview');

          </script>


    </head>  
    <body id="start_nicdark_framework">
        <div class="nicdark_site" id="home"> 
            <div class="nicdark_site_fullwidth nicdark_clearfix">
                <?php $this->load->view('includes/template/header'); ?>
                <?php $this->load->view($view); ?>
                <?php $this->load->view('includes/template/footer'); ?>
                <!--end section-->        
            </div>
        </div>        
        <?php $this->load->view('includes/template/scripts'); ?>
    </body>  
</html>
