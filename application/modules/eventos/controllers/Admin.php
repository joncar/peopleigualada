<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }
                
        function eventos(){
            $crud = $this->crud_function('','');
            $crud->set_field_upload('foto','uploads/eventos');
            $crud = $crud->render();
            $this->loadView($crud);
        }
    }
?>
