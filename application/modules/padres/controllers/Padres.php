<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Padres extends Panel{
        CONST _GRUPO_ = 2;
        function __construct() {
            parent::__construct();
        }
        
        function user(){
            $crud = $this->crud_function('','');
            $crud->where('admin = 0','ESCAPE',FALSE);
            $crud->columns('cedula','nombre','apellido','email','status');
            $crud->display_as('cedula','DNI')
                 ->display_as('apellido','Apellidos')            
                 ->field_type('fecha_registro','hidden',date("Y-m-d"))
                 ->field_type('fecha_actualizacion','hidden',date("Y-m-d"))
                 ->field_type('foto','hidden','');
            $crud->unset_fields('admin');
            $crud->callback_before_insert(function($post){
                $post['password'] = md5($post['password']);
                return $post;
            });
            $crud->callback_after_insert(function($post,$primary){
                get_instance()->db->insert('user_group',array('user'=>$primary,'grupo'=>self::_GRUPO_));            
            });
            $crud->callback_before_update(function($post,$primary){
                if(get_instance()->db->get_where('user',array('id'=>$primary))->row()->password!=$post['password'])
                $post['password'] = md5($post['password']);
                return $post;
            });
            if($crud->getParameters()=='add'){
                $crud->set_rules('cedula','DNI','required|numeric|is_unique[user.cedula]');
                $crud->set_rules('email','Email','required|valid_email|is_unique[user.email]');
                $crud->field_type('status','hidden',1);
            }else{
                $crud->field_type('status','dropdown',array('0'=>'Bloqueado','1'=>'Activo'));
            }
            $this->loadView($crud->render());
        }
    }
?>
