<section id="nicdark_parallax_countdown" class="nicdark_section nicdark_imgparallax nicdark_parallaxx_img-events">

    <div class="nicdark_filter greydark">

        <!--start nicdark_container-->
        <div class="nicdark_container nicdark_clearfix">

            <div class="nicdark_space40"></div>
            <div class="nicdark_space50"></div>
            <div class="nicdark_space100"></div>

            <div class="grid grid_12">
                <h1 class="white center subtitle">ZONA PARES !</h1>
                <div class="nicdark_space10"></div>
                <h3 class="subtitle center white">ARE YOU READY ?</h3>
                <div class="nicdark_space20"></div>
                <div class="nicdark_divider center big"><span class="nicdark_bg_white nicdark_radius"></span></div> 
                <div class="nicdark_space20"></div>    
            </div>

            <div class="nicdark_space40"></div>
            <div class="nicdark_space50"></div>

        </div>
        <!--end nicdark_container-->

    </div>

</section>
<!--end section-->

<!--start section-->
<section class="nicdark_section">

    <!--start nicdark_container-->
    <div class="nicdark_container nicdark_clearfix">


        <div class="nicdark_space50"></div>


        <div class="grid grid_12">
            <h1 class="subtitle greydark">ÚLTIMES NOTICIES</h1>
            <div class="nicdark_space20"></div>
            <h3 class="subtitle grey">ESTIGUEU INFORMATS AMB PEOPLE IGUALADA</h3>
            <div class="nicdark_space20"></div>
            <div class="nicdark_divider left big"><span class="nicdark_bg_green nicdark_radius"></span></div>
            <div class="nicdark_space10"></div>
        </div>
        <?php foreach($detail->result() as $d): ?>
            <div class="grid grid_3">
                <!--archive1-->
                <div class="nicdark_archive1 nicdark_bg_green nicdark_radius nicdark_shadow">

                    <a href="<?= $d->link ?>" class="nicdark_btn nicdark_bg_greydark white medium nicdark_radius nicdark_absolute_left"><?= date("d",strtotime($d->fecha)) ?><br/>
                        <small><?= strtoupper(substr(strftime("%B",strtotime($d->fecha)),0,3)) ?></small> 
                    </a>

                    <img alt=""  src="<?= $d->foto ?>">

                    <div class="nicdark_textevidence nicdark_bg_greydark">
                        <h4 class="white nicdark_margin20"><?= $d->titulo ?></h4>
                    </div>

                    <div class="nicdark_margin20">                        
                        <div class="nicdark_space20"></div>
                        <p class="white"><?= substr(strip_tags($d->texto),0,80).'...' ?></p>
                        <div class="nicdark_space20"></div>
                        <a href="<?= $d->link ?>" class="nicdark_press nicdark_btn nicdark_bg_greendark white nicdark_radius nicdark_shadow medium">Veure Més</a>
                    </div>
                </div>
                <!--archive1-->
            </div>
        <?php endforeach ?>
        
    </div>
    <!--end nicdark_container-->

</section>
<!--end section-->


<!--start section-->
<div class="nicdark_section">

    <!--start nicdark_container-->
    <div class="nicdark_container nicdark_clearfix">

        <div class="nicdark_space40"></div>

        <div class="grid grid_6 nicdark_aligncenter_iphoneland nicdark_aligncenter_iphonepotr">
            <a href="<?= base_url('blog') ?>?page=<?= empty($_GET['page'])?'1':$_GET['page']-1; ?>" class="nicdark_disable_floatright_iphoneland nicdark_disable_floatright_iphonepotr nicdark_btn nicdark_bg_green medium right nicdark_shadow nicdark_radius white nicdark_press">
                <i class="icon-left-open-outline"></i>&nbsp;&nbsp;&nbsp;ANTERIORS
            </a>
        </div>
        <div class="grid grid_6 nicdark_aligncenter_iphoneland nicdark_aligncenter_iphonepotr">
            <a href="<?= base_url('blog') ?>?page=<?= empty($_GET['page'])?'2':$_GET['page']+1; ?>" class=" nicdark_btn nicdark_bg_green medium nicdark_shadow nicdark_radius white nicdark_press">
                PRÓXIMES&nbsp;&nbsp;&nbsp;<i class="icon-right-open-outline"></i>
            </a>
        </div>

        <div class="nicdark_space50"></div>

    </div>
    <!--end nicdark_container-->

</div>
<!--end section--><!--end-->
