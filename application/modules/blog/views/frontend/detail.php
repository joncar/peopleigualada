<section id="nicdark_parallax_title" class="nicdark_section nicdark_imgparallax nicdark_parallaxx_img8">

    <div class="nicdark_filter greydark">

        <!--start nicdark_container-->
        <div class="nicdark_container nicdark_clearfix">

            <div class="grid grid_12">
                <div class="nicdark_space100"></div>
                <div class="nicdark_space100"></div>
                <h1 class="white subtitle"><?= $detail->titulo ?></h1>
                <div class="nicdark_space10"></div>                
                <div class="nicdark_divider left big"><span class="nicdark_bg_white nicdark_radius"></span></div>
                <div class="nicdark_space40"></div>
                <div class="nicdark_space50"></div>
            </div>

        </div>
        <!--end nicdark_container-->

    </div>

</section>
<!--end section-->
<!--start section-->
<section class="nicdark_section">
    <!--start nicdark_container-->
    <div class="nicdark_container nicdark_clearfix">
        <div class="nicdark_space50"></div>
        <div class="grid grid_8">
            <img alt="" class="nicdark_radius nicdark_opacity" style="float:left;width:100%;" src="<?= $detail->foto ?>">
            <div class="nicdark_space20"></div>
            <div class="nicdark_textevidence nicdark_bg_orange nicdark_radius nicdark_shadow">
                <div class="nicdark_size_big">
                    <p class="white">
                        <i class="icon-calendar-1 nicdark_marginright10"></i>
                        <?= strtolower(strftime("%d %B")) ?> 
                        <span class="nicdark_margin010">·</span> 
                        <i class="icon-user-1 nicdark_marginright10"></i><?= $detail->user ?>
                    </p>
                </div>
                <i class="icon-info-outline nicdark_iconbg right medium orange"></i>
            </div>
            <div class="nicdark_space50"></div>
            <h1 class="subtitle greydark"><?= $detail->titulo ?></h1>
            <div class="nicdark_space20"></div>
            <div class="nicdark_divider left small"><span class="nicdark_bg_yellow nicdark_radius"></span></div>
            <div class="nicdark_space20"></div>
            <p>
                <?= $detail->texto ?>
            </p>
            <div class="nicdark_space50"></div>
        </div>

        <!--sidebar-->
        <div class="grid grid_4">

            <div class="nicdark_archive1 nicdark_bg_grey nicdark_radius nicdark_shadow">
                <div class="nicdark_textevidence nicdark_bg_yellow nicdark_radius_top">
                    <h4 class="white nicdark_margin20">T'AJUDEM A TROBAR</h4>
                    <i class="icon-search-outline nicdark_iconbg right medium yellow"></i>
                </div>
                <div class="nicdark_margin20">
                    <form action="<?= base_url('blog') ?>" method="get">
                        <input name="direccion" class="nicdark_bg_grey2 nicdark_radius nicdark_shadow grey small subtitle" type="text" value="" size="200" placeholder="Escriu lo que busques">
                        <div class="nicdark_space20"></div>
                        <input class="nicdark_btn nicdark_bg_yellow medium nicdark_shadow nicdark_radius white" type="submit" value="Buscar">
                    </form>
                </div>
            </div>

            <div class="nicdark_space20"></div>

            <div class="nicdark_archive1 nicdark_bg_grey nicdark_radius nicdark_shadow">
                <div class="nicdark_textevidence nicdark_bg_orange nicdark_radius_top">
                    <h4 class="white nicdark_margin20">TAG</h4>
                    <i class="icon-tags-1 nicdark_iconbg right medium orange"></i>
                </div>
                <div class="nicdark_margin10">
                    <?php foreach(explode(',',$detail->tags) as $e): ?>
                    <a href="<?= base_url('blog') ?>direccion=<?= $e ?>" class="nicdark_btn nicdark_bg_grey2 small nicdark_shadow nicdark_radius grey subtitle nicdark_margin10">
                        <?= $e ?>
                    </a>
                    <?php endforeach ?>
                </div>
            </div>

            <div class="nicdark_space20"></div>

            <div class="nicdark_archive1 nicdark_bg_grey nicdark_radius nicdark_shadow">
                <div class="nicdark_textevidence nicdark_bg_violet nicdark_radius_top">
                    <h4 class="white nicdark_margin20">NOTCIES RELACIONADES</h4>
                    <i class="icon-doc-1 nicdark_iconbg right medium violet"></i>
                </div>
                <ul class="nicdark_list border">
                    <?php foreach($relacionados->result() as $r): ?>
                        <li class="nicdark_border_grey">
                            <a href="<?= $r->link ?>">
                                <div class="nicdark_margin20 nicdark_relative">
                                    <img alt="" class="nicdark_absolute nicdark_radius" style="width:60px;" src="<?= $r->foto ?>">
                                    <div class="nicdark_activity nicdark_marginleft80">
                                        <h5 class="grey"><?= $r->titulo ?></h5>                        
                                        <div class="nicdark_space10"></div>
                                        <p><?= substr(strip_tags($r->texto),0,100) ?></p>
                                    </div>
                                </div>
                            </a>
                        </li>
                    <?php endforeach ?>
                </ul>
            </div>

        </div>
        <!--sidebar-->

        <div class="nicdark_space50"></div>

    </div>
    <!--end nicdark_container-->

</section>
<!--end section-->

<!--end-->

<div class="nicdark_space3 nicdark_bg_gradient"></div>
