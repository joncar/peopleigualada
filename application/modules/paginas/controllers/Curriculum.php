<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Curriculum extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
            $this->load->library('form_validation');
        }
        
        function send(){                 
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nom','Nombre','required');
            $this->form_validation->set_rules('message','Comentario','required');
            if($this->form_validation->run()){
                if(!empty($_FILES['curriculum']['tmp_name'])){
                    get_instance()->load->library('mailer');
                    get_instance()->mailer->mail->AddAttachment($_FILES['curriculum']['tmp_name'],$_FILES['curriculum']['name']);
                    $this->enviarcorreo((object)$_POST,3,'info@peopleigualada.com');
                    /*get_instance()->mailer->mail->clearAttachments();
                    $this->enviarcorreo((object)$_POST,5);*/
                    $data = array();
                    $data['nombre'] = $_POST['nom'];
                    $data['email'] = $_POST['email'];
                    /*$data['telefono'] = $_POST['telefon'];*/
                    $data['comentario'] = $_POST['message'];
                    $data['adjunto'] = date("ydmhis").$_FILES['curriculum']['name'];
                    $this->db->insert('curriculums',$data);
                    move_uploaded_file($_FILES['curriculum']['tmp_name'],'uploads/curriculums/'.$data['adjunto']);
                    $_SESSION['msj'] = $this->success('El teu projecte s’ha enviat amb éxit. En breu contactarem amb tu. Gràcies');
                }else{
                    $_SESSION['msj'] = $this->error('Has oblidat adjuntar el curriculum, sis plau fes servir PDF, jpg, png. Sis <script>$("#guardar").attr("disabled",false); </script>');
                }
            }else{
                $_SESSION['msj'] = $this->error('plau completa les dades demanades. <script>$("#guardar").attr("disabled",false); </script>');
            }            
            redirect(base_url().'#curriculum');
        }
    }
