<section class="nicdark_section firstsection">

    <div class="tp-banner-container">
        <div class="nicdark_slide1" >

            <ul>


                <!--start first-->
                <li data-transition="kenburn" data-slotamount="4" data-masterspeed="100" data-saveperformance="on"  data-title="">
                    <img src="<?= base_url() ?>img/slide/img3.jpg"  alt="" data-lazyload="<?= base_url() ?>img/slide/img3.jpg" data-bgposition="center bottom" data-bgfit="cover" data-bgrepeat="no-repeat">
                </li>
                <!--end first-->


                <!--start second-->
                <li data-transition="fade" data-slotamount="4" data-masterspeed="200" data-saveperformance="on"  data-title="">
                    <img src="<?= base_url() ?>img/parallax/img-single-teacher-1.jpg"  alt="" data-lazyload="<?= base_url() ?>img/parallax/img-single-teacher-1.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                </li>
                <!--end second-->

                <!--start 3-->
                <li data-transition="kenburn" data-slotamount="4" data-masterspeed="300" data-saveperformance="on"  data-title="">
                    <img src="<?= base_url() ?>img/parallax/img8.jpg"  alt="" data-lazyload="<?= base_url() ?>img/parallax/img8.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                </li>
                <!--end second-->

                <!--start 4-->
                <li data-transition="fade" data-slotamount="4" data-masterspeed="400" data-saveperformance="on"  data-title="">
                    <img src="<?= base_url() ?>img/parallax/img7.jpg"  alt="" data-lazyload="<?= base_url() ?>img/parallax/img7.jpg" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                </li>
                <!--end second-->

            </ul>

        </div>
    </div>

</section>
<!--end section--><!--start section-->
<section class="nicdark_section nicdark_margintop45_negative">

    <!--start nicdark_container-->
    <div class="nicdark_container nicdark_clearfix">

        <div class="grid grid_12 percentage nomargin">
            <div class="nicdark_textevidence center">
                <div class="nicdark_textevidence nicdark_width_percentage25 nicdark_bg_blue nicdark_shadow nicdark_radius_left">
                    <div class="nicdark_textevidence" >
                        <div class="nicdark_margin30">
                            <h3 class="white subtitle"><a class="white" style="font-family: 'Raleway', sans-serif;">❝With languages, you are at home anywhere.❞ <br>– <br>
                                    <h5 class="signature"><a class="white" style="font-family: 'Montez', sans-serif; font-size: 25px">Edward De Waal</a></h5>

                                </a></h3>
                        </div>
                        <i class="nicdark_zoom icon-picture-outline  nicdark_iconbg left extrabig blue nicdark_displaynone_ipadland nicdark_displaynone_ipadpotr"></i>
                    </div>
                </div>
                <div class="nicdark_textevidence nicdark_width_percentage25 nicdark_bg_yellow nicdark_shadow">
                    <div class="nicdark_textevidence" style="margin: 0.6px;">
                        <div class="nicdark_margin30">
                            <h3 class="white subtitle"><a class="white" style="font-family: 'Raleway', sans-serif;">❝The limits of my language are the limits of my world.❞ <br>– </a></h3>
                            <h5 class="white subtitle"><a class="white" style="font-family: 'Montez', sans-serif; font-size: 25px">Ludwig Wittgenstein </a></h5>
                        </div>
                        <i class="nicdark_zoom  icon-globe-alt-outline nicdark_iconbg left extrabig yellow nicdark_displaynone_ipadland nicdark_displaynone_ipadpotr"></i>
                    </div>
                </div>
                <div class="nicdark_textevidence nicdark_width_percentage25 nicdark_bg_orange nicdark_shadow">
                    <div class="nicdark_textevidence">
                        <div class="nicdark_margin30">
                            <h3 class="white subtitle"><a class="white" style="font-family: 'Raleway', sans-serif;">❝Knowledge of languages is the doorway to wisdom.❞ <br>– 
                                    <h5 class="white subtitle"><a class="white" style="font-family: 'Montez', sans-serif; font-size: 25px">Roger Bacon</a></h5>
                                </a></h3>
                        </div>
                        <i class="nicdark_zoom icon-pencil-2 nicdark_iconbg left extrabig orange nicdark_displaynone_ipadland nicdark_displaynone_ipadpotr"></i>
                    </div>
                </div>
                <div class="nicdark_textevidence nicdark_width_percentage25 nicdark_bg_green nicdark_shadow nicdark_radius_right">
                    <div class="nicdark_textevidence">
                        <div class="nicdark_margin30">
                            <h3 class="white subtitle" style="margin: -12.2px;"><a class="white" style="font-family: 'Raleway', sans-serif;">❝The work of education is divided between the teacher and the environment.❞
                                    <br>– 
                                    <h5 class="white subtitle" ><a class="white" style="font-family: 'Montez', sans-serif; font-size: 25px">Maria Montessori</a></h5>
                                </a></h3>
                        </div>
                        <i class="nicdark_zoom icon-graduation-cap-1 nicdark_iconbg left extrabig green nicdark_displaynone_ipadland nicdark_displaynone_ipadpotr"></i>
                    </div>
                </div>
                <div class="nicdark_space5"></div>
            </div>
        </div>

    </div>
    <!--end nicdark_container-->

</section>
<!--end section--><!--start section-->
<section class="nicdark_section" id="quisom">
    <!--start nicdark_container-->
    <div class="quisomHome quisomdiv nicdark_container nicdark_clearfix">
        <div class="nicdark_space50"></div>
        <div class="grid grid_6">            
            <h1 class="subtitle greydark"style=" font-weight: 400">qui som?</h1>
            <div class="nicdark_space20"></div>
            <div class="nicdark_divider left small"><span class="nicdark_bg_yellow nicdark_radius"></span></div>
            <div class="nicdark_space20"></div>
            <p>
                <span class="white nicdark_dropcap nicdark_bg_yellow nicdark_radius nicdark_shadow">P</span>
                eople va néixer l'any 2000 com a idea d'una mestra i el seu equip, que després de treballar a Educació
                Secundària des de 1995, va decidir a partir de la seva experiència, d'empendre una nova aventura i aplicar
                una metodologia pro-activa en base a l'atenció a la diversitat i on la motivació i el progrés de l'alumne fos el seu
                motor de treball.
            </p>
            <div class="nicdark_space20"></div>
            <p style="color: rgba(10, 24, 49, 0.83);">
                <span class="white nicdark_dropcap nicdark_bg_yellow nicdark_radius nicdark_shadow">P</span>
                eople was founded in year 2000 by a team of teachers who, having worked in Secondary Education since 1995, decided to embark on a new endeavour using a diverse and pro-active methodology. This innovative approach uses the students' progress itself as a source of motivation and inspiration.
            </p>
        </div>
        <!--sidebar-->
        <div class="grid grid_6 supermandentro">
            <img src="<?= base_url() ?>img/detall.png">
        </div>
        <!--sidebar-->        
    </div>
    <!--end nicdark_container-->
     <div class="grid grid_6 supermanfuera">
        <img src="<?= base_url() ?>img/detall.png">
    </div>
</section>
<!--end section--><!--start section-->

<section id="nicdark_parallax_text" class="nicdark_section nicdark_imgparallax nicdark_parallaxx_img-single-teacher-2">

    <div class="nicdark_filter blue">

        <!--start nicdark_container-->
        <div class="nicdark_container nicdark_clearfix">

            <div class="grid grid_12">
                <div class="nicdark_space40"></div>
                <div class="nicdark_space50"></div>
                <h2 class="white subtitle center">❝One language sets you in a corridor for life. Two languages open every door along the way.❞
                </h2>
                <div class="nicdark_space20"></div>
                <div class="nicdark_divider big center"><span class="nicdark_bg_white nicdark_radius"></span></div>
                <div class="nicdark_space20"></div>
                <h1 class="signature white center">Frank Smith</h1>
                <div class="nicdark_space40"></div>
                <div class="nicdark_space50"></div>
            </div>

        </div>
        <!--end nicdark_container-->

    </div>

</section>

<!--start section-->
<section class="nicdark_section" id="metodologia">

    <!--start nicdark_container-->
    <div class="nicdark_container nicdark_clearfix">

        <div class="nicdark_space50"></div>


        <div class="grid grid_12">
            <h1 class="subtitle greydark"style=" font-weight: 400">metodologia </h1>
            <div class="nicdark_space20"></div>
            <h3 class="subtitle grey">Com aprenem a la nostra escola?</h3>
            <div class="nicdark_space20"></div>
            <div class="nicdark_divider left big"><span class="nicdark_bg_green nicdark_radius"></span></div>
            <div class="nicdark_space10"></div>
        </div>

        <div class="grid grid_4">
            <img alt="" class="nicdark_radius nicdark_opacity" style="float:left;width:100%;" src="img/team/img9.jpg">

            <div class="nicdark_space10"></div>

            <!--<div class="nicdark_focus center">
                <div class="nicdark_margin10">
                    <a title="YOUTUBE" href="#" class="nicdark_press nicdark_tooltip right nicdark_btn_icon nicdark_bg_red nicdark_shadow small nicdark_radius white"><i class="icon-youtube-play"></i></a>
                </div>
                <div class="nicdark_margin10">
                    <a title="DRIBBBLE" href="#" class="nicdark_press nicdark_tooltip right nicdark_btn_icon nicdark_bg_violet nicdark_shadow small nicdark_radius white"><i class="icon-dribbble-1"></i></a>
                </div>
                <div class="nicdark_margin10">
                    <a title="TWITTER" href="#" class="nicdark_press nicdark_tooltip right nicdark_btn_icon nicdark_bg_blue nicdark_shadow small nicdark_radius white"><i class="icon-twitter-1"></i></a>
                </div>
            </div>-->
        </div>

        <div class="grid grid_4">
            <h3 class="subtitle greydark">VISIÓ</h3>
            <div class="nicdark_space20"></div>
            <div class="nicdark_divider left small"><span class="nicdark_bg_blue nicdark_radius"></span></div>
            <div class="nicdark_space20"></div>
            <p>Creiem en una metodologia on la llengua sigui el mitjà per a treballar el centre d'interès de l'alumne, que desperti el seu esperit crític, que s'adapti al seu ritme d'aprenentatge i que basada en la diversitat, tingui en compte les característiques emocionals i cognitives de cadascú. Som un equip que treballa amb una visió holística de l'ensenyament. </p>
            <br><p style="
    color: rgba(10, 24, 49, 0.83);
">We believe in a theme-based cross-curricular methodology where language learning is not the aim but the means which helps to develop critical thinking and autonomous learning. The learning process is adapted to each student and thus takes into account the emotional and cognitive characteristics of every individual, as well as the pace of learning. Our team works with a holistic teaching approach.
            </p>
        </div>


        <div class="grid grid_4">
            <h3 class="subtitle greydark">APRENENTATGE CONSTRUCTIVISTA</h3>
            <div class="nicdark_space20"></div>
            <div class="nicdark_divider left small"><span class="nicdark_bg_yellow nicdark_radius"></span></div>
            <div class="nicdark_space20"></div>

            <div class="nicdark_progressbar nicdark_bg_grey nicdark_radius nicdark_shadow">
                <h5 class="nicdark_progressbar_title nicdark_bg_yellow nicdark_bg_yellowdark_hover nicdark_radius nicdark_shadow fade-left animate1 animated fadeInLeft" style="width:100%">
                    <span class="white nicdark_size_big"><i class="icon-award"></i>&nbsp;&nbsp;&nbsp;MOTIVACIÓ </span>
                </h5>
            </div>

            <div class="nicdark_space20"></div>

            <div class="nicdark_progressbar nicdark_bg_grey nicdark_radius nicdark_shadow">
                <h5 class="nicdark_progressbar_title nicdark_bg_blue nicdark_bg_bluedark_hover nicdark_radius nicdark_shadow fade-left animate2 animated fadeInLeft" style="width:100%">
                    <span class="white nicdark_size_big"><i class="icon-pencil-1"></i>&nbsp;&nbsp;&nbsp;PERSONALITZACIÓ </span>
                </h5>
            </div>

            <div class="nicdark_space20"></div>

            <div class="nicdark_progressbar nicdark_bg_grey nicdark_radius nicdark_shadow">
                <h5 class="nicdark_progressbar_title nicdark_bg_green nicdark_bg_greendark_hover nicdark_radius nicdark_shadow fade-left animate3 animated fadeInLeft" style="width:100%">
                    <span class="white nicdark_size_big"><i class="icon-smile"></i>&nbsp;&nbsp;&nbsp;EMOCIONS </span>
                </h5>
            </div>

            <div class="nicdark_space20"></div>

            <div class="nicdark_progressbar nicdark_bg_grey nicdark_radius nicdark_shadow">
                <h5 class="nicdark_progressbar_title nicdark_bg_orange nicdark_radius nicdark_bg_orangedark_hover nicdark_shadow fade-left animate4 animated fadeInLeft" style="width:100%">
                    <span class="white nicdark_size_big"><i class="icon-puzzle"></i>&nbsp;&nbsp;&nbsp;CREATITIVITAT </span>
                </h5>
            </div>

            <div class="nicdark_space20"></div>

            <div class="nicdark_progressbar nicdark_bg_grey nicdark_radius nicdark_shadow">
                <h5 class="nicdark_progressbar_title nicdark_bg_red nicdark_bg_reddark_hover nicdark_radius nicdark_shadow fade-left animate4 animated fadeInLeft" style="width:100%">
                    <span class="white nicdark_size_big"><i class="icon-up-hand"></i>&nbsp;&nbsp;&nbsp;ESPERIT CRÍTIC </span>
                </h5>
            </div>    

        </div>

        <div class="nicdark_space50"></div>

    </div>
    <!--end nicdark_container-->

</section>
<!--end section-->


<section id="nicdark_parallax_counter" class="nicdark_section nicdark_imgparallax nicdark_parallax_img1">

    <div class="nicdark_filter greydark">

         <!--start nicdark_container-->
        <div class="nicdark_container nicdark_clearfix">

            <div class="grid grid_12">
                <div class="nicdark_space40"></div>
                <div class="nicdark_space50"></div>
                <h2 class="white subtitle center">❝If children feel safe, they can take risks, ask questions, make mistakes, learn to trust, share their feelings, and grow.❞
                </h2>
                <div class="nicdark_space20"></div>
                <div class="nicdark_divider big center"><span class="nicdark_bg_white nicdark_radius"></span></div>
                <div class="nicdark_space20"></div>
                <h1 class="signature white center">Alfie Kohn</h1>
                <div class="nicdark_space40"></div>
                <div class="nicdark_space50"></div>
            </div>

        </div>
        <!--end nicdark_container-->

    </div>

</section>

<!--end section--><!--start section-->
<section class="nicdark_section" id="faq">

    <!--start nicdark_container-->
    <div class="nicdark_container nicdark_clearfix" style=" margin-bottom: 90px">


        <div class="nicdark_space50"></div>

        <div class="grid grid_12">
            <h1 class="subtitle greydark"style=" font-weight: 400">FAQ</h1>
            <div class="nicdark_space20"></div>
            <h3 class="subtitle grey">Preguntes freqüents</h3>
            <div class="nicdark_space20"></div>
            <div class="nicdark_divider left big"><span class="nicdark_bg_blue nicdark_radius"></span></div>
            <div class="nicdark_space10"></div>
        </div>


        <div class="nicdark_masonry_btns">
           <!--<div class="nicdark_margin10">
                <a data-filter="*" class="nicdark_bg_grey2_hover nicdark_transition nicdark_btn nicdark_bg_grey small nicdark_shadow nicdark_radius grey">TOTES</a>
            </div>-->
            <div class="nicdark_margin10">
                <a data-filter=".educational" class="nicdark_bg_grey2_hover nicdark_transition nicdark_btn nicdark_bg_grey small nicdark_shadow nicdark_radius grey">PARES</a>
            </div>
            <div class="nicdark_margin10">
                <a data-filter=".excursions" class="nicdark_bg_grey2_hover nicdark_transition nicdark_btn nicdark_bg_grey small nicdark_shadow nicdark_radius grey">ALUMNES</a>
            </div>
          <!--  <div class="nicdark_space10"></div>
            <div class="nicdark_margin10">
                <a data-filter=".excursions" class="nicdark_bg_grey2_hover nicdark_transition nicdark_btn nicdark_bg_grey small nicdark_shadow nicdark_radius grey">QÜALIFICIONS</a>
            </div>-->
            <div class="nicdark_space10"></div>
        </div>


        <!--start nicdark_masonry_container-->
        <div class="nicdark_masonry_container">


            <div class="grid grid_4 nicdark_masonry_item educational">
                <div class="nicdark_archive1 nicdark_bg_blue nicdark_radius nicdark_shadow">

                    <a href="single-post-right-sidebar.html" class="nicdark_zoom nicdark_btn_icon nicdark_bg_blue nicdark_border_bluedark white medium nicdark_radius_circle nicdark_absolute_left"><i class=" icon-emo-coffee"></i></a>

                    <img alt=""  src="<?= base_url() ?>img/blog/img1.jpg">

                    <div class="nicdark_margin20">
                        <h4 class="white">QUINA ÉS LA MILLOR EDAT PER A INICIAR L'APRENENTATGE D'UNA LLENGUA ESTRANGERA?</h4>
                        <div class="nicdark_space20"></div>
                        <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
                        <div class="nicdark_space20"></div>
                        <p class="white">
                            La recerca i els estudis sobre aquest tema constaten que començar l'aprenentatge d'una segona llengua estrangera a la infància facilita en general la seva pronunciació i adquisició a nivell de proficiència. Tot i així, hi ha molts altres factors que influeixen en l'adquisició d'una llengua i començar molt aviat no és cap garantia d'èxit.
                            Alguns neurolingüistes afirmaren en les seves teories que existeix un període òptim per l'adquisició d'una segona llengua estrangera -Critical Period Theory- que s'inicia a la primera infància. Si més no, lingüistes i estudis posteriors provaren que la maduresa cognitiva de l'alumne i el fet d'haver desenvolupat estratègies d'aprenentatge fa que l'adquisició d'una llengua estrangera sigui més eficient en edat escolar, i no abans. És per aquest motiu, que als països nòrdics l'aprenentatge de la llengua anglesa s'inicia de forma intensiva un cop l'alumne és capaç d'assolir la lecto-escriptura a primària. <br>La nostra experiència avala les últimes teories i per això recomanem d'iniciar l'aprenentatge cap als sis anys.</p>
                        <div class="nicdark_space20"></div>
                        <a href="p/contacto.html" class="white nicdark_btn"><i class="icon-doc-text-1 "></i> Consultar</a>
                    </div>

                    <i class="icon-camera-outline nicdark_iconbg right medium blue"></i>

                </div>


            </div>
            <div class="grid grid_4 nicdark_masonry_item educational">
                <div class="nicdark_archive1 nicdark_bg_green nicdark_radius nicdark_shadow">

                    <a href="single-post-right-sidebar.html" class="nicdark_zoom nicdark_btn_icon nicdark_bg_green nicdark_border_greendark white medium nicdark_radius_circle nicdark_absolute_left"><i class="icon-emo-coffee"></i></a>

                    <img alt=""  src="<?= base_url() ?>img/blog/img20.jpg">

                    <div class="nicdark_margin20">
                        <h4 class="white">FINS A QUINA EDAT ES POT APRENDRE UNA LLENGUA?
                        </h4>
                        <div class="nicdark_space20"></div>
                        <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
                        <div class="nicdark_space20"></div>
                        <p class="white">Malgrat que la neurolíngüística és molt controvertida,  sembla que l'edat òptima d'aprenentatge d'un idioma és fins a la pubertat però també cal tenir en compte molts altres factors a part d'aquest. Tot i que la capacitat i el ritme d'aprenentatge disminueix amb l'edat, sempre es pot aprendre una llengua.
                        </p>
                        <div class="nicdark_space20"></div>
                        <a href="p/contacto.html" class="white nicdark_btn"><i class="icon-doc-text-1 "></i> Consultar</a>
                    </div>

                    <i class="icon-pencil-1 nicdark_iconbg right medium green"></i>

                </div>
            </div>




            <div class="grid grid_4 nicdark_masonry_item educational">
                <div class="nicdark_archive1 nicdark_bg_violet nicdark_radius nicdark_shadow">

                    <a href="single-post-right-sidebar.html" class="nicdark_zoom nicdark_btn_icon nicdark_bg_violet nicdark_border_violetdark white medium nicdark_radius_circle nicdark_absolute_left"><i class="icon-emo-coffee"></i></a>

                    <img alt=""  src="<?= base_url() ?>img/blog/21.jpg">

                    <div class="nicdark_margin20">
                        <h4 class="white">ELS MESTRES HAN DE SER NADIUS?
                        </h4>
                        <div class="nicdark_space20"></div>
                        <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
                        <div class="nicdark_space20"></div>
                        <p class="white">Un professor ha de dominar la matèria, però més important encara és que l'ha de saber transmetre i ha d'il.lusionar els alumnes. Cal prioritzar que sigui un bon mestre però també hem tenir garanties del seu nivell de proficiència sigui nadiu o no nadiu. És cert que només un professor nadiu podrà cobrir certs aspectes lingüístics.</p>
                        <div class="nicdark_space20"></div>
                        <a href="p/contacto.html" class="white nicdark_btn"><i class="icon-doc-text-1 "></i> Consultar</a>
                    </div>

                    <i class="icon-picture-outline nicdark_iconbg right medium violet"></i>

                </div>
            </div>




            <div class="grid grid_4 nicdark_masonry_item educational">
                <div class="nicdark_archive1 nicdark_bg_orange nicdark_radius nicdark_shadow">

                    <a href="single-post-right-sidebar.html" class="nicdark_zoom nicdark_btn_icon nicdark_bg_orange nicdark_border_orangedark white medium nicdark_radius_circle nicdark_absolute_left"><i class="icon-emo-coffee"></i></a>

                    <img alt=""  src="<?= base_url() ?>img/blog/22.jpg">

                    <div class="nicdark_margin20">
                        <h4 class="white">QUINA QUALIFICACIÓ HAURIEN DE TENIR ELS MESTRES NADIUS I NO NADIUS?
                        </h4>
                        <div class="nicdark_space20"></div>
                        <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
                        <div class="nicdark_space20"></div>
                        <p class="white">Caldria demanar que un professor nadiu tingui la titulació CELTA o bé DELTA o equivalent per a fer de mestre i seria desitjable que un professor no-nadiu, a més de la titulació universitària, hagi assolit el nivell C2 i acceptable el nivell C1. Una titulació universitària no és garantia avui per avui, que es domini l'idioma amb proficiència, segons la nostra experiència.
                        </p>
                        <div class="nicdark_space20"></div>
                        <a href="p/contacto.html" class="white nicdark_btn"><i class="icon-doc-text-1 "></i> Consultar</a>
                    </div>


                    <i class="icon-camera-outline nicdark_iconbg right medium orange"></i>

                </div>
            </div>



            <div class="grid grid_4 nicdark_masonry_item educational">
                <div class="nicdark_archive1 nicdark_bg_red nicdark_radius nicdark_shadow">

                    <a href="single-post-right-sidebar.html" class="nicdark_zoom nicdark_btn_icon nicdark_bg_red nicdark_border_reddark white medium nicdark_radius_circle nicdark_absolute_left"><i class="icon-emo-coffee"></i></a>

                    <img alt=""  src="<?= base_url() ?>img/excursions/img6.jpg">

                    <div class="nicdark_margin20">
                        <h4 class="white">ÉS SUFICIENT UN NIVELL B2 PER A ENSENYAR UN IDIOMA, ESPECIALMENT A INFANTS QUAN EL NIVELL ÉS BAIX?
                        </h4>
                        <div class="nicdark_space20"></div>
                        <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
                        <div class="nicdark_space20"></div>
                        <p class="white">Fins ara els Graus en Magisteri amb especialitat llengua anglesa i Llengues Modernes exigien acabar la universitat amb el nivell B2. A partir d'ara, s'exigirà el nivell C1.
                            L'aprenentatge a primària és especialment important perquè es produeix intuïtivament. L'alumne  calcarà la pronunciació, gramàtica, vocabulari i expressions del mestre i per tant també n'assimilarà els errors. En el nostre criteri i experiència, un nivell B2 no és suficient.
                        </p>
                        <div class="nicdark_space20"></div>
                        <a href="p/contacto.html" class="white nicdark_btn"><i class="icon-doc-text-1 "></i> Consultar</a>
                    </div>

                    <i class="icon-pencil-1 nicdark_iconbg right medium red"></i>

                </div>
            </div>
            <div class="grid grid_4 nicdark_masonry_item educational">
                <div class="nicdark_archive1 nicdark_bg_green nicdark_radius nicdark_shadow">

                    <a href="single-post-right-sidebar.html" class="nicdark_zoom nicdark_btn_icon nicdark_bg_green nicdark_border_greendark white medium nicdark_radius_circle nicdark_absolute_left"><i class="icon-emo-coffee"></i></a>

                    <img alt=""  src="<?= base_url() ?>img/blog/img17.jpg">

                    <div class="nicdark_margin20">
                        <h4 class="white">COM PUC CONTROLAR EL PROGRÉS DEL MEU FILL SI JO NO SÉ ANGLÈS?
                        </h4>
                        <div class="nicdark_space20"></div>
                        <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
                        <div class="nicdark_space20"></div>
                        <p class="white">Estarem encantats de resoldre personalment aquestes i d'altres preguntes i us animem a fer recerca sobre aquests i d'altres temes abans de decidir com, quan i on aprendrà anglès el vostre fill/a.
                        <div class="nicdark_space20"></div>
                        <a href="p/contacto.html" class="white nicdark_btn"><i class="icon-doc-text-1 "></i> Consultar</a>
                    </div>

                    <i class="icon-picture-outline nicdark_iconbg right medium green"></i>

                </div>
            </div>
            <div class="grid grid_4 nicdark_masonry_item educational">
                <div class="nicdark_archive1 nicdark_bg_yellow nicdark_radius nicdark_shadow">

                    <a href="single-post-right-sidebar.html" class="nicdark_zoom nicdark_btn_icon nicdark_bg_yellow nicdark_border_yellowdark white medium nicdark_radius_circle nicdark_absolute_left"><i class="icon-emo-coffee"></i></a>

                    <img alt=""  src="<?= base_url() ?>img/blog/img11.jpg">

                    <div class="nicdark_margin20">
                        <h4 class="white">PER QUÈ UNA METODOLOGIA NO ÉS SEMPRE ADEQUADA PER A TOTHOM?
                        </h4>
                        <div class="nicdark_space20"></div>
                        <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
                        <div class="nicdark_space20"></div>
                        <p class="white">Estarem encantats de resoldre personalment aquestes i d'altres preguntes i us animem a fer recerca sobre aquests i d'altres temes abans de decidir com, quan i on aprendrà anglès el vostre fill/a.
                        <div class="nicdark_space20"></div>
                        <a href="p/contacto.html" class="white nicdark_btn"><i class="icon-doc-text-1 "></i> Consultar</a>
                    </div>

                    <i class="icon-picture-outline nicdark_iconbg right medium yellow"></i>

                </div>
            </div>




            <div class="grid grid_4 nicdark_masonry_item educational">
                <div class="nicdark_archive1 nicdark_bg_blue nicdark_radius nicdark_shadow">

                    <a href="single-post-right-sidebar.html" class="nicdark_zoom nicdark_btn_icon nicdark_bg_blue nicdark_border_bluedark white medium nicdark_radius_circle nicdark_absolute_left"><i class="icon-emo-coffee"></i></a>

                    <img alt=""  src="<?= base_url() ?>img/blog/img9.jpg">

                    <div class="nicdark_margin20">
                        <h4 class="white">A QUINA EDAT HA DE COMENÇAR A ESCRIURE EN ANGLÈS EL MEU FILL/A?
                        </h4>
                        <div class="nicdark_space20"></div>
                        <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
                        <div class="nicdark_space20"></div>
                        <p class="white">Estarem encantats de resoldre personalment aquestes i d'altres preguntes i us animem a fer recerca sobre aquests i d'altres temes abans de decidir com, quan i on aprendrà anglès el vostre fill/a.
                        <div class="nicdark_space20"></div>
                        <a href="p/contacto.html" class="white nicdark_btn"><i class="icon-doc-text-1 "></i> Consultar</a>
                    </div>

                    <i class="icon-camera-outline nicdark_iconbg right medium blue"></i>

                </div>
            </div>

            <div class="grid grid_4 nicdark_masonry_item educational">
                <div class="nicdark_archive1 nicdark_bg_orange nicdark_radius nicdark_shadow">

                    <a href="single-post-right-sidebar.html" class="nicdark_zoom nicdark_btn_icon nicdark_bg_orange nicdark_border_orangedark white medium nicdark_radius_circle nicdark_absolute_left"><i class="icon-emo-coffee"></i></a>

                    <img alt=""  src="<?= base_url() ?>img/blog/img10.jpg">

                    <div class="nicdark_margin20">
                        <h4 class="white">COM PUC AJUDAR AL MEU FILL A APRENDRE ANGLÈS?
                        </h4>
                        <div class="nicdark_space20"></div>
                        <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
                        <div class="nicdark_space20"></div>
                        <p class="white">Estarem encantats de resoldre personalment aquestes i d'altres preguntes i us animem a fer recerca sobre aquests i d'altres temes abans de decidir com, quan i on aprendrà anglès el vostre fill/a.
                        <div class="nicdark_space20"></div>
                        <a href="p/contacto.html" class="white nicdark_btn"><i class="icon-doc-text-1 "></i> Consultar</a>
                    </div>

                    <i class="icon-picture-outline nicdark_iconbg right medium orange"></i>

                </div>
            </div>

            <div class="grid grid_4 nicdark_masonry_item educational">
                <div class="nicdark_archive1 nicdark_bg_red nicdark_radius nicdark_shadow">

                    <a href="single-post-right-sidebar.html" class="nicdark_zoom nicdark_btn_icon nicdark_bg_orange nicdark_border_orangedark white medium nicdark_radius_circle nicdark_absolute_left"><i class="icon-emo-coffee"></i></a>

                    <img alt=""  src="<?= base_url() ?>img/blog/23.jpg">


                    <div class="nicdark_margin20">
                        <h4 class="white">QUINS EXÀMENS OFICIALS D'ANGLÈS HI HA? QUINES ENTITATS ELS ORGANITZEN I QUANTS NIVELLS HI HA? QUAN I QUINS SÓN IMPORTANTS DE REALITZAR?
                        </h4>
                        <div class="nicdark_space20"></div>
                        <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
                        <div class="nicdark_space20"></div>
                        <p class="white">Estarem encantats de resoldre personalment aquestes i d'altres preguntes i us animem a fer recerca sobre aquests i d'altres temes abans de decidir com, quan i on aprendrà anglès el vostre fill/a.
                        <div class="nicdark_space20"></div>
                        <a href="p/contacto.html" class="white nicdark_btn"><i class="icon-doc-text-1 "></i> Consultar</a>
                    </div>

                    <i class="icon-pencil-1 nicdark_iconbg right medium red"></i>

                </div>
            </div>




            <div class="grid grid_4 nicdark_masonry_item educational">
                <div class="nicdark_archive1 nicdark_bg_red nicdark_radius nicdark_shadow">

                    <a href="single-post-right-sidebar.html" class="nicdark_zoom nicdark_btn_icon nicdark_bg_orange nicdark_border_orangedark white medium nicdark_radius_circle nicdark_absolute_left"><i class="icon-emo-coffee"></i></a>

                    <img alt=""  src="<?= base_url() ?>img/blog/img12.jpg">


                    <div class="nicdark_margin20">
                        <h4 class="white">QUAN PUC ENVIAR AL MEU FILL A UNA ESTADA A L'ESTRANGER?


                        </h4>
                        <div class="nicdark_space20"></div>
                        <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
                        <div class="nicdark_space20"></div>
                        <p class="white">Estarem encantats de resoldre personalment aquestes i d'altres preguntes i us animem a fer recerca sobre aquests i d'altres temes abans de decidir com, quan i on aprendrà anglès el vostre fill/a.
                        </p>
                        <div class="nicdark_space20"></div>
                        <a href="p/contacto.html" class="white nicdark_btn"><i class="icon-doc-text-1 "></i> Consultar</a>
                    </div>

                    <i class="icon-pencil-1 nicdark_iconbg right medium red"></i>

                </div>
            </div>

            <div class="grid grid_4 nicdark_masonry_item excursions">
                <div class="nicdark_archive1 nicdark_bg_green nicdark_radius nicdark_shadow">

                    <a href="single-post-right-sidebar.html" class="nicdark_zoom nicdark_btn_icon nicdark_bg_blue nicdark_border_bluedark white medium nicdark_radius_circle nicdark_absolute_left"><i class="icon-emo-happy"></i></a>

                    <img alt=""  src="<?= base_url() ?>img/blog/img13.jpg">

                    <div class="nicdark_margin20">
                        <h4 class="white">FINS A QUINA EDAT ES POT APRENDRE UNA LLENGUA?
                        </h4>
                        <div class="nicdark_space20"></div>
                        <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
                        <div class="nicdark_space20"></div>
                        <p class="white">Malgrat que la neurolíngüística és molt controvertida,  sembla que l'edat òptima d'aprenentatge d'un idioma és fins a la pubertat però també cal tenir en compte molts altres factors a part d'aquest. Tot i que la capacitat i el ritme d'aprenentatge disminueix amb l'edat, sempre es pot aprendre una llengua.
                        </p>
                        <div class="nicdark_space20"></div>
                        <a href="p/contacto.html" class="white nicdark_btn"><i class="icon-doc-text-1 "></i> Consultar</a>
                    </div>

                    <i class="icon-pencil-1 nicdark_iconbg right medium green"></i>

                </div>
            </div>


            <div class="grid grid_4 nicdark_masonry_item educational">
                <div class="nicdark_archive1 nicdark_bg_red nicdark_radius nicdark_shadow">

                    <a href="single-post-right-sidebar.html" class="nicdark_zoom nicdark_btn_icon nicdark_bg_orange nicdark_border_orangedark white medium nicdark_radius_circle nicdark_absolute_left"><i class="icon-emo-coffee"></i></a>

                    <img alt=""  src="<?= base_url() ?>img/blog/img14.jpg">


                    <div class="nicdark_margin20">
                        <h4 class="white"> ACADÈMIA O ESCOLA D'IDIOMES?

                        </h4>
                        <div class="nicdark_space20"></div>
                        <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
                        <div class="nicdark_space20"></div>
                        <p class="white">Estarem encantats de resoldre personalment aquestes i d'altres preguntes i us animem a fer recerca sobre aquests i d'altres temes abans de decidir com, quan i on aprendrà anglès el vostre fill/a.
                        </p>
                        <div class="nicdark_space20"></div>
                        <a href="p/contacto.html" class="white nicdark_btn"><i class="icon-doc-text-1 "></i> Consultar</a>
                    </div>

                    <i class="icon-pencil-1 nicdark_iconbg right medium red"></i>

                </div>
            </div>

            <div class="grid grid_4 nicdark_masonry_item excursions">
                <div class="nicdark_archive1 nicdark_bg_violet nicdark_radius nicdark_shadow">

                    <a href="single-post-right-sidebar.html" class="nicdark_zoom nicdark_btn_icon nicdark_bg_violet nicdark_border_violetdark white medium nicdark_radius_circle nicdark_absolute_left"><i class="icon-emo-happy"></i></a>

                    <img alt=""  src="<?= base_url() ?>img/blog/21.jpg">

                    <div class="nicdark_margin20">
                        <h4 class="white">ELS MESTRES HAN DE SER NADIUS?
                        </h4>
                        <div class="nicdark_space20"></div>
                        <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
                        <div class="nicdark_space20"></div>
                        <p class="white">Un professor ha de dominar la matèria, però més important encara és que l'ha de saber transmetre i ha d'il.lusionar els alumnes. Cal prioritzar que sigui un bon mestre però també hem tenir garanties del seu nivell de proficiència sigui nadiu o no nadiu. És cert que només un professor nadiu podrà cobrir certs aspectes lingüístics.</p>
                        <div class="nicdark_space20"></div>
                        <a href="p/contacto.html" class="white nicdark_btn"><i class="icon-doc-text-1 "></i> Consultar</a>
                    </div>

                    <i class="icon-picture-outline nicdark_iconbg right medium violet"></i>

                </div>
            </div>

            <div class="grid grid_4 nicdark_masonry_item excursions">
                <div class="nicdark_archive1 nicdark_bg_yellow nicdark_radius nicdark_shadow">

                    <a href="single-post-right-sidebar.html" class="nicdark_zoom nicdark_btn_icon nicdark_bg_yellow nicdark_border_yellowdark white medium nicdark_radius_circle nicdark_absolute_left"><i class="icon-emo-happy"></i></a>

                    <img alt=""  src="<?= base_url() ?>img/blog/img11.jpg">

                    <div class="nicdark_margin20">
                        <h4 class="white">PER QUÈ UNA METODOLOGIA NO ÉS SEMPRE ADEQUADA PER A TOTHOM?
                        </h4>
                        <div class="nicdark_space20"></div>
                        <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
                        <div class="nicdark_space20"></div>
                        <p class="white">Estarem encantats de resoldre personalment aquestes i d'altres preguntes i us animem a fer recerca sobre aquests i d'altres temes abans de decidir com, quan i on aprendrà anglès el vostre fill/a.
                        <div class="nicdark_space20"></div>
                        <a href="p/contacto.html" class="white nicdark_btn"><i class="icon-doc-text-1 "></i> Consultar</a>
                    </div>

                    <i class="icon-picture-outline nicdark_iconbg right medium yellow"></i>

                </div>
            </div>

            <div class="grid grid_4 nicdark_masonry_item excursions">
                <div class="nicdark_archive1 nicdark_bg_orange nicdark_radius nicdark_shadow">

                    <a href="single-post-right-sidebar.html" class="nicdark_zoom nicdark_btn_icon nicdark_bg_orange nicdark_border_orangedark white medium nicdark_radius_circle nicdark_absolute_left"><i class="icon-emo-happy"></i></a>

                    <img alt=""  src="<?= base_url() ?>img/blog/img17.jpg">

                    <div class="nicdark_margin20">
                        <h4 class="white">QUINA QUALIFICACIÓ HAURIEN DE TENIR ELS MESTRES NADIUS I NO NADIUS?
                        </h4>
                        <div class="nicdark_space20"></div>
                        <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
                        <div class="nicdark_space20"></div>
                        <p class="white">Caldria demanar que un professor nadiu tingui la titulació CELTA o bé DELTA o equivalent per a fer de mestre i seria desitjable que un professor no-nadiu, a més de la titulació universitària, hagi assolit el nivell C2 i acceptable el nivell C1. Una titulació universitària no és garantia avui per avui, que es domini l'idioma amb proficiència, segons la nostra experiència.
                        </p>
                        <div class="nicdark_space20"></div>
                        <a href="p/contacto.html" class="white nicdark_btn"><i class="icon-doc-text-1 "></i> Consultar</a>
                    </div>


                    <i class="icon-camera-outline nicdark_iconbg right medium orange"></i>

                </div>
            </div>


            <div class="grid grid_4 nicdark_masonry_item excursions">
                <div class="nicdark_archive1 nicdark_bg_red nicdark_radius nicdark_shadow">

                    <a href="single-post-right-sidebar.html" class="nicdark_zoom nicdark_btn_icon nicdark_bg_orange nicdark_border_orangedark white medium nicdark_radius_circle nicdark_absolute_left"><i class="icon-emo-happy"></i></a>

                    <img alt=""  src="<?= base_url() ?>img/blog/23.jpg">


                    <div class="nicdark_margin20">
                        <h4 class="white">QUINS EXÀMENS OFICIALS D'ANGLÈS HI HA? QUINES ENTITATS ELS ORGANITZEN I QUANTS NIVELLS HI HA? QUAN I QUINS SÓN IMPORTANTS DE REALITZAR?
                        </h4>
                        <div class="nicdark_space20"></div>
                        <div class="nicdark_divider left small"><span class="nicdark_bg_white nicdark_radius"></span></div>
                        <div class="nicdark_space20"></div>
                        <p class="white">Estarem encantats de resoldre personalment aquestes i d'altres preguntes i us animem a fer recerca sobre aquests i d'altres temes abans de decidir com, quan i on aprendrà anglès el vostre fill/a.
                        <div class="nicdark_space20"></div>
                        <a href="p/contacto.html" class="white nicdark_btn"><i class="icon-doc-text-1 "></i> Consultar</a>
                    </div>

                    <i class="icon-pencil-1 nicdark_iconbg right medium red"></i>

                </div>
            </div>

        </div>
        <!--end nicdark_masonry_container-->

    </div>
    <!--end nicdark_container-->

</section>
<!--end section-->

<section id="nicdark_parallax_counter" class="nicdark_section nicdark_imgparallax nicdark_parallaxx_img-teachers-2">

    <div class="nicdark_filter greydark" style="
    background: rgba(172, 122, 181, 0.65);
">

         <!--start nicdark_container-->
        <div class="nicdark_container nicdark_clearfix" >

            <div class="grid grid_12">
                <div class="nicdark_space40"></div>
                <div class="nicdark_space50"></div>
                <h2 class="white subtitle center">❝Teaching is not about answering questions but about raising questions – opening doors for them in places that they could not imagine.❞
                </h2>
                <div class="nicdark_space20"></div>
                <div class="nicdark_divider big center"><span class="nicdark_bg_white nicdark_radius"></span></div>
                <div class="nicdark_space20"></div>
                <h1 class="signature white center">Yawar Baig</h1>
                <div class="nicdark_space40"></div>
                <div class="nicdark_space50"></div>
            </div>

        </div>
        <!--end nicdark_container-->

    </div>

</section>


<section id="testimonio" class="nicdark_section nicdark_imgparallax nicdark_parallaxx_img-teachers-1">
    <div class="nicdark_filter blue">

        <!--start nicdark_container-->
        <div class="nicdark_container nicdark_clearfix">
            <div class="grid grid_2"></div> 
            <div class="grid grid_7">
                <div class="nicdark_space50"></div>
                <div class="testsection nicdark_textevidence nicdark_bg_white nicdark_radius">
                    <h3 class="subtitle greydark" style=" font-weight: 700">I TU QUÈ EN PENSES?</h3>
                    <div class="nicdark_space20"></div>
                    <div class="nicdark_divider left small"><span class="nicdark_bg_green nicdark_radius"></span></div>                    
                    
                    <ul class="owl">
                        
                        <?php foreach($this->db->get('testimonios')->result() as $t): ?>
                            <li>
                                    <div class="nicdark_space20"></div>
                                    <div class="nicdark_textevidence nicdark_bg_green nicdark_radius nicdark_shadow">
                                        <div class="nicdark_margin20">
                                            <p class="white"><?= $t->testimonio ?></p>
                                        </div>
                                        <i class="icon-quote-right nicdark_iconbg right medium green"></i>
                                    </div>
                                    <div class="nicdark_focus"><div class="nicdark_triangle green nicdark_marginleft20"></div></div>
                                    <div class="nicdark_space30"></div>
                                    <div class="nicdark_focus nicdark_relative">
                                        <img width="65" src="<?= base_url('img/testimonios/'.$t->icono) ?>" class="nicdark_radius_circle nicdark_absolute" alt="">

                                        <div class="nicdark_activity nicdark_marginleft85">
                                            <h5 style="
    line-height: 10.5px;
"><?= $t->autor ?>
                                                <a class="likes" title="likes" id="likes<?= $t->id ?>" href="javascript:sumarCorazon('<?= $t->id ?>')"><br/><br/>
                                                    <i class="icon-thumbs-up"></i>  <?= count(json_decode($t->likes)) ?>
                                                </a>
                                            </h5>
                                    </div>


                                    </div>
                            </li>
                        <?php endforeach ?>
                        
                    </ul>
                </div>
                <div class="nicdark_space50"></div>
            </div>

        </div>
        <!--end nicdark_container-->

    </div>
</section>
<section class="nicdark_section" id="curriculum">
    <!--start nicdark_container-->
    <div class="nicdark_container nicdark_clearfix">
        <div class="nicdark_space50"></div>
        <div class="grid grid_6">            
            <h1 class="subtitle greydark" >WOULD YOU LIKE TO WORK WITH US?</h1>
            <div class="nicdark_space20"></div>
            <div class="nicdark_divider left small"><span class="nicdark_bg_yellow nicdark_radius"></span></div>
            <div class="nicdark_space20"></div>
            <p><span class="white nicdark_dropcap nicdark_bg_yellow nicdark_radius nicdark_shadow">I</span>f you fulfil de requirements and are a motivated native English teacher who truly enjoys teaching English with an enthusiastic, dynamic and professional approach and you'd like to join our team of teachers, please, send us a message. We will be delighted to meet you.

            </p> <br> <br>
            <p style="color: rgba(10, 24, 49, 0.83);" ><span class="white nicdark_dropcap nicdark_bg_yellow nicdark_radius nicdark_shadow">S</span>i tens la titulació i ets un mestre motivat amb ganes de viure i transmetre el teu entusiasme als teus alumnes i vols treballar amb nosaltres, envia'ns un missatge. Estarem encantats de conèixer-te.


            </p>
        </div>
        <!--sidebar-->
        <div class="grid grid_6">                                              
            <img src="<?= base_url() ?>img/vols.jpg" style="width:100%;    border-radius: 8px;">
        </div>
    </div>
    <!--end nicdark_container-->
</section>
<!--end section-->

<!--start section-->
<section class="nicdark_section">

    <!--start nicdark_container-->
    <div class="nicdark_container nicdark_clearfix">

        <div class="grid grid_12">
            <h3 class="subtitle greydark">UPLOAD YOUR C.V.</h3>
            <div class="nicdark_space20"></div>
            <div class="nicdark_divider left small"><span class="nicdark_bg_green nicdark_radius"></span></div>
            <h3 class="greydark"><?php echo @$_SESSION['msj']; $_SESSION['msj'] = ''; ?></h3>
        </div>
        <div class="grid grid_12 nicdark_bg_grey nicdark_shadow nicdark_radius nicdark_relative">

            <a class="nicdark_displaynone_iphoneland nicdark_displaynone_iphonepotr nicdark_btn_icon nicdark_bg_green  extrabig nicdark_radius_left white nicdark_absolute" href="#"><i class="icon-upload-1"></i></a>
            <form action="<?= base_url('paginas/curriculum/send') ?>" method="post" enctype="multipart/form-data">
                <div class="nicdark_textevidence">
                    <div class="curriculumsection nicdark_margin1820 nicdark_marginleft100 nicdark_marginleft20_iphoneland nicdark_marginleft20_iphonepotr">
                        <div class="nicdark_focus nicdark_width_percentage25">
                            <input name="nom" type="text" size="200" value="" placeholder="Name"  class="nicdark_bg_grey2 nicdark_radius nicdark_shadow grey medium subtitle">
                        </div>
                        <div class="nicdark_focus nicdark_width_percentage25">
                            <div class="nicdark_marginleft20 nicdark_disable_marginleft_iphoneland nicdark_disable_marginleft_iphonepotr">
                                <input name="email" type="text" size="200" value="" placeholder="Email" class="nicdark_bg_grey2 nicdark_radius nicdark_shadow grey medium subtitle">
                            </div>
                        </div>
                        <div class="nicdark_focus nicdark_width_percentage25">
                            <div class="nicdark_marginleft20 nicdark_disable_marginleft_iphoneland nicdark_disable_marginleft_iphonepotr" style="height: 46px;"> 
                                <label class="nicdark_bg_grey2 nicdark_radius nicdark_shadow grey medium subtitle nicdark_calendar hasDatepicker fileuploadbutton">Upload<input name="curriculum" type="file" style="display:none"></label>
                            </div>       
                        </div>
                        <div class="nicdark_focus nicdark_width_percentage25 submitcurriculum" style="margin-bottom: -2.3px;">
                            <div class="nicdark_marginleft20 nicdark_disable_marginleft_iphoneland nicdark_disable_marginleft_iphonepotr">
                                <input type="submit" value="SEND" class="nicdark_btn fullwidth nicdark_bg_green medium nicdark_shadow nicdark_radius white">
                            </div>
                        </div>
                        <div class="nicdark_space20" id="formcurriculumspace"></div>
                        <input type="text" name="message" size="200" value="" placeholder="Write your application letter" class="nicdark_bg_grey2 nicdark_radius nicdark_shadow grey medium subtitle nicdark_width_percentage60"style=" padding: 10px 1%; width: 98%;">
                    </div>
                </div>
            </form>
        </div>



        <div class="nicdark_space50"></div>


    </div>
    <!--end nicdark_container-->

</section>
<!--end section-->

<!--start section-->
<section class="nicdark_section">
    <div class="nicdark_textevidence" id="flat-map"  data-zoom='18' data-lat="41.5810362" data-lon="1.628413499999965" data-direccion="C/ Gardènies, 20. 08700 Igualada" data-icon="http://peopleigualada.com/img/icon.png" style="height:300px">

    </div>
</section>
<!--end section--><!--end-->
<?php $this->load->view('includes/template/modals'); ?>
