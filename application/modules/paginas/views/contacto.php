<!--start section-->
<section id="nicdark_parallax_title" class="nicdark_section nicdark_imgparallax nicdark_parallaxx_img5">

    <div class="nicdark_filter greydark">

        <!--start nicdark_container-->
        <div class="nicdark_container nicdark_clearfix">

            <div class="grid grid_12">
                <div class="nicdark_space100"></div>
                <div class="nicdark_space100"></div>
                <h1 class="white subtitle">CONTACTE</h1>
                <div class="nicdark_space10"></div>
                <h3 class="subtitle white">FEEL FREE TO CONTACT US</h3>
                <div class="nicdark_space20"></div>
                <div class="nicdark_divider left big"><span class="nicdark_bg_white nicdark_radius"></span></div>
                <div class="nicdark_space40"></div>
                <div class="nicdark_space50"></div>
            </div>

        </div>
        <!--end nicdark_container-->

    </div>

</section>
<!--end section-->


<!--start section-->
<section class="nicdark_section">

    <!--start nicdark_container-->
    <div class="nicdark_container nicdark_clearfix">

        <div class="nicdark_space40"></div>


        <div class="grid grid_4">

            <div class="nicdark_archive1 nicdark_bg_grey nicdark_radius nicdark_shadow">
                <div class="nicdark_margin20 nicdark_relative">
                    <a href="#" class="nicdark_displaynone_ipadpotr nicdark_btn_icon nicdark_bg_yellow extrabig nicdark_radius_circle white nicdark_absolute nicdark_shadow"><i class="icon-clock nicdark_rotate"></i></a>
                    <div class="nicdark_activity nicdark_marginleft100 nicdark_disable_marginleft_ipadpotr">
                        <h4>HORARI</h4>
                        <div class="nicdark_space20"></div>
                        <p>De dilluns a divendres <br>de 15 a 20.30h</p>
                        <p>Secretaria: De dilluns a divendres <br>de 17 a 20h.</p>
                        </p>
                    </div>
                </div>
            </div>

            <div class="nicdark_space20"></div>


            <div class="nicdark_archive1 nicdark_bg_grey nicdark_radius nicdark_shadow">
                <div class="nicdark_margin20 nicdark_relative">
                    <a href="#" class="nicdark_displaynone_ipadpotr nicdark_btn_icon nicdark_bg_orange extrabig nicdark_radius_circle white nicdark_absolute nicdark_shadow"><i class="icon-address nicdark_rotate"></i></a>
                    <div class="nicdark_activity nicdark_marginleft100 nicdark_disable_marginleft_ipadpotr">
                        <h4>ADREÇA</h4>
                        <div class="nicdark_space20"></div>
                        <p>C/ Gardènies, 20. 08700 Igualada </p>
                        <p>Telf: 93 8045005</p>
                    </div>
                </div>
            </div>

            <div class="nicdark_space20"></div>

            <div class="nicdark_archive1 nicdark_bg_grey nicdark_radius nicdark_shadow">
                <div class="nicdark_margin20 nicdark_relative">
                    <a id="contacto-icono" href="#" class="nicdark_displaynone_ipadpotr nicdark_btn_icon nicdark_bg_red extrabig nicdark_radius_circle white nicdark_absolute nicdark_shadow">
                        <i class="icon-paper-plane-empty nicdark_rotate"></i>
                    </a>
                    <div id="contacto-email" class="nicdark_activity nicdark_marginleft100 nicdark_disable_marginleft_ipadpotr">
                        <h4>M@IL US</h4>
                        <div class="nicdark_space20"></div>
                        <a href="mailto:secretaria@peopleigualada.com" style="text-decoration: underline;">
                            secretaria@peopleigualada.com
                        </a>
                    </div>
                </div>
            </div>

            <div class="nicdark_space20"></div>
        </div>


        <div class="grid grid_8">

            <div class="nicdark_archive1 nicdark_bg_grey nicdark_radius nicdark_shadow">
                <div class="nicdark_textevidence nicdark_bg_orange nicdark_radius_top">
                    <h4 class="white nicdark_margin20">CONTACTAR</h4>
                    <i class="icon-mail nicdark_iconbg right medium orange"></i>
                </div>
                <div class="nicdark_margin20">                    
                    <form action="<?= base_url('paginas/frontend/contacto') ?>" method="post">                        
                        <input name="nombre" class="nicdark_bg_grey2 nicdark_radius nicdark_shadow grey small subtitle" type="text" value="" placeholder="NOM">
                        <div class="nicdark_space20"></div>
                        <input name="email" class="nicdark_bg_grey2 nicdark_radius nicdark_shadow grey small subtitle" type="text" value="" placeholder="EMAIL">
                        <div class="nicdark_space20"></div>
                        <textarea name="mensaje" class="nicdark_bg_grey2 nicdark_radius nicdark_shadow grey small subtitle" placeholder="MISSATGE" rows="7"></textarea>
                        <div class="nicdark_space20"></div>
                        <div style="width:100%; display: inline-block;"><?php echo @$_SESSION['msj']; $_SESSION['msj'] = ''; ?>
                        <input class="nicdark_btn nicdark_bg_orange medium nicdark_shadow nicdark_radius white" type="submit" value="ENVIAR">                                                
                    </form>
                </div>
            </div>

        </div>




        <div class="nicdark_space50"></div>


    </div>
    <!--end nicdark_container-->

</section>
<!--end section-->

<!--start section-->
<section class="nicdark_section">
    <div class="nicdark_textevidence" id="flat-map"  data-zoom='18' data-lat="41.5810362" data-lon="1.628413499999965" data-direccion="Avda. Mestre Montaner, 86. 08700 Igualada, Barcelona" data-icon="http://peopleigualada.com/img/icon.png" style="height:300px">

    </div>
</section>
