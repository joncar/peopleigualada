<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct();
        }

        function ajustes(){
        	$crud = $this->crud_function("","");
        	$crud->unset_delete()->unset_add()->unset_print()->unset_export()->unset_read();
        	$crud = $crud->render();
        	$this->loadView($crud);
        }
    }
?>
