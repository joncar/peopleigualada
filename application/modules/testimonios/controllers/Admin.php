<?php 
    require_once APPPATH.'controllers/Panel.php';    
    class Admin extends Panel{
        function __construct() {
            parent::__construct(); 
        }
        
        function testimonios(){
            $crud = $this->crud_function("","");
            $crud->unset_fields('likes')
                     ->unset_columns('likes');
            $crud->field_type('testimonio','editor',array('type'=>'textarea'));
            $crud->set_field_upload('icono','img/testimonios');
            $crud = $crud->render();
            $this->loadView($crud);
        }
        
        function sumarCorazon($id){
            $proyecto = $this->db->get_where('proyectos',array('id'=>$id));
            if($proyecto->num_rows()>0){
                $likes = empty($proyecto->row()->likes)?array():json_decode($proyecto->row()->likes);
                $ip = getUserIP();
                if(!in_array($ip,$likes)){
                    $likes[] = $ip;
                }else{
                    foreach($likes as $n=>$v){
                        if($v==$ip){
                            unset($likes[$n]);
                        }
                    }
                }
                $count = count($likes);
                $this->db->update('proyectos',array('likes'=>json_encode($likes)),array('id'=>$id));
                echo $count;
            }else{
                echo 'fail';
            }
        }
    }
?>
