<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
            $this->load->library('form_validation');
        }        
        function sumarCorazon($id){
            $proyecto = $this->db->get_where('testimonios',array('id'=>$id));
            if($proyecto->num_rows()>0){
                $likes = empty($proyecto->row()->likes)?array():json_decode($proyecto->row()->likes);
                $likes = (array)$likes;
                $ip = getUserIP();
                
                if(!in_array($ip,$likes)){
                    $likes[] = $ip;
                }else{
                    foreach($likes as $n=>$v){
                        if($v==$ip){
                            unset($likes[$n]);
                        }
                    }
                }
                $count = count($likes);
                $this->db->update('testimonios',array('likes'=>json_encode($likes)),array('id'=>$id));
                echo $count;
            }else{
                echo 'fail';
            }
        }
    }
