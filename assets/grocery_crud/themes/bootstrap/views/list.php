<?php if(!empty($list)): ?>
<script>
    angular_grocery_crud.filter('startFrom', function() {
        return function(input, start) {
            start = +start; //parse to int
            return input.slice(start);
        }
    });

        angular_grocery_crud.controller('grocery_crud_list',function($scope){
        <?php 
           if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)){               
               foreach($list as $n=>$l){
                   $list[$n]->actions = '';
                   if(!$unset_read){
                        $list[$n]->actions.= '<a href="'.$l->read_url.'" title="'.$this->l('list_view').$subject.'" class="edit_button"><i class="glyphicon glyphicon-list-alt"></i></a> ';
                   }
                   if(!$unset_edit){
                        $list[$n]->actions.= '<a style="color:blue" href="'.$l->edit_url.'" title="'.$this->l('list_edit').$subject.'" class="edit_button"><i class="glyphicon glyphicon-edit"></i></a> ';
                   }
                   if(!$unset_delete){
                        $list[$n]->actions.= '<a style=color:red href="'.$l->delete_url.'" title="'.$this->l('list_delete').$subject.'" class="delete-row" ><i class="glyphicon glyphicon-remove"></i></a> ';
                   }
               }
           }
        ?>
        $scope.list = <?= json_encode($list) ?>;
        $scope.itemsPerPage = 25;
        $scope.currentPage = 1;
        $scope.order = $scope.list[0][0];
        $scope.reverse = true;        
        
    
        //Concurrencia
        $scope.initialize = function(){
            $.post('<?= $ajax_list_info_url ?>',{},function(data){
                data = JSON.parse(data);
                $scope.total_length = data.total_results;
                $scope.last_page = data.total_results/1000;
                $scope.list = [];
                $scope.get_results();
            });
            
        }
        
        $scope.get_results = function(){
            $("#search_label").addClass('loading glyphicon-refresh');
            $("#search_label").removeClass('glyphicon-search');
            $.post('<?= $json_list_url ?>',{page:$scope.init_page,per_page:1000},function(data){
                data = JSON.parse(data);
                for(i in data){
                    $scope.list.push(data[i]);
                    console.log($scope.list);
                }
                
                if($scope.init_page<$scope.last_page){
                    $scope.init_page++;
                    $scope.get_results();
                }
                else{
                    $("#search_label").addClass('glyphicon-search');
                    $("#search_label").removeClass('loading glyphicon-refresh');
                }
            });
        }
        
        
        $scope.init_page = 1;
        $scope.last_page = 0;
        $scope.total_length = 0;
        
        
        
        $scope.initialize();
        
        
        $scope.change_order= function(attr){
            $scope.order = attr;
            $scope.reverse = !$scope.reverse;
            $scope.reverse?
            $("#th_"+attr).html('<i class="glyphicon glyphicon-chevron-up"></i>'):
            $("#th_"+attr).html('<i class="glyphicon glyphicon-chevron-down"></i>');
        }

        $scope.pageChanged = function(no) {
          $scope.currentPage = no;
        };
    });
</script>
<?php 
    $column_width = (int)(80/count($columns));    
?>
<div style="overflow: auto">
<table class="table table-bordered table-condensed table-striped">                        
    <thead>
            <tr>                    
                    <?php foreach($columns as $column){?>
                    <th style='cursor:pointer;' ng-click="change_order('<?= $column->field_name ?>')">                        
                        <?php echo $column->display_as?>
                        <span id="th_<?= $column->field_name ?>"></span>
                    </th>
                    <?php }?>
                    <?php if(!$unset_delete || !$unset_edit || !$unset_read || !empty($actions)){?>
                    <th align="right" abbr="tools" axis="col1" class="" width='20%'>
                            <div class="text-right">
                                    <?php echo $this->l('list_actions'); ?>
                            </div>
                    </th>
                    <?php }?>
            </tr>
    </thead>		
    <tbody>
        <tr ng-repeat="i in filteredTodos = (list | filter:query | orderBy:order:reverse) | startFrom:(currentPage-1)*itemsPerPage | limitTo:itemsPerPage">
            <?php foreach($columns as $n=>$column):?>
            <td ng-bind-html="i.<?= $column->field_name ?> | to_trusted"></td>
            <?php endforeach ?>
            <td align="right" ng-bind-html="i.actions | to_trusted"></td>
        </tr>
    </tbody>
</table>
</div>
<?php else: ?>
Sin datos para mostrar
<?php endif; ?>
<div class="form-group">
    <label class="sr-only" for="Search"><i class="glyphicon glyphicon-search"></i></label>
    <div class="input-group">
        <div class="input-group-addon"><i id="search_label" class="glyphicon glyphicon-search"></i></div>
        <input type="text" class="form-control" id="exampleInputAmount" placeholder="Parametro a buscar" ng-model="query">      
    </div>
</div>
<pagination next-text=">" last-text=">>" first-text="<<" previous-text='<' boundary-links="true" max-size="10" items-per-page="itemsPerPage" total-items="filteredTodos.length" ng-model="currentPage"></pagination>

